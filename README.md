# ConferenceCalendar


With this app you can easily and quickly book conference rooms.

Author: Stefan Häling

PHP: 7.1


Based on laravel, Microsoft Graph API.

    https://docs.microsoft.com/de-de/graph/overview

For LIVE testing:

    http://conferencecalendar.herokuapp.com

Developers:

    git clone git@bitbucket.org:bestit-haeling/conferencecalendar.git
    
    composer install
    
    php artisan serve