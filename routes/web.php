<?php

Route::get('/', 'Outlook\WelcomeController@welcome');

Route::get('/authorize', 'AuthController@gettoken');

Route::get('/booking/{location}/{room}', 'Outlook\BookingController@booking')->name('quickbooking');

Route::get('/roomSelect', 'Outlook\RoomSelectController@roomSelect')->name('roomSelect');

Route::get('/roomsVelen', 'Outlook\Velen\VelenCalendarController@roomsVelen')->name('roomsVelen');

Route::get('/selectLocation', 'Outlook\LocationSelectBookingController@selectLocation')->name('selectLocation');

Route::get('/selectVelen', 'Outlook\Velen\VelenRoomSelectController@selectVelen')->name('selectVelen');

Route::get('/signin', 'AuthController@signin');

Route::get('/signout', 'AuthController@signout');

Route::get('/users', 'Outlook\UsersController@users')->name('users');

Route::get('/mailertest', 'MailController@sendMail')->name('sendMail');
