<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\TokenStore\TokenCache;
use Illuminate\Http\RedirectResponse;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Class AuthController
 *
 * @author Stefan Häling <stefan.haeling@bestit-online.de>
 *
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * Signin method
     *
     * @return void
     */
    public function signin(): void
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        // Initialize the OAuth client
        $oauthClient = new GenericProvider([
            'clientId'                => env('OAUTH_APP_ID'),
            'clientSecret'            => env('OAUTH_APP_PASSWORD'),
            'redirectUri'             => env('OAUTH_REDIRECT_URI'),
            'urlAuthorize'            => env('OAUTH_AUTHORITY') . env('OAUTH_AUTHORIZE_ENDPOINT'),
            'urlAccessToken'          => env('OAUTH_AUTHORITY') . env('OAUTH_TOKEN_ENDPOINT'),
            'urlResourceOwnerDetails' => '',
            'scopes'                  => env('OAUTH_SCOPES')
        ]);

        // Generate the auth URL
        $authorizationUrl = $oauthClient->getAuthorizationUrl();

        // Save client state so we can validate in response
        $_SESSION['oauth_state'] = $oauthClient->getState();

        // Redirect to authorization endpoint
        header('Location: ' . $authorizationUrl);
        exit();
    }

    /**
     * @return RedirectResponse
     */
    public function signout(): RedirectResponse
    {
        $tokenCache = new TokenCache();
        $tokenCache->clearTokens();
        return redirect('/');
    }

    /**
     * @return RedirectResponse
     */
    public function gettoken(): RedirectResponse
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        try{
            // Authorization code should be in the "code" query param
            if (isset($_GET['code'])) {
                // Check that state matches
                if (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth_state'])) {
                    exit('State provided in redirect does not match expected value.');
                }

                // Clear saved state
                unset($_SESSION['oauth_state']);

                // Initialize the OAuth client
                $oauthClient = new GenericProvider([
                    'clientId'                => env('OAUTH_APP_ID'),
                    'clientSecret'            => env('OAUTH_APP_PASSWORD'),
                    'redirectUri'             => env('OAUTH_REDIRECT_URI'),
                    'urlAuthorize'            => env('OAUTH_AUTHORITY') . env('OAUTH_AUTHORIZE_ENDPOINT'),
                    'urlAccessToken'          => env('OAUTH_AUTHORITY') . env('OAUTH_TOKEN_ENDPOINT'),
                    'urlResourceOwnerDetails' => '',
                    'scopes'                  => env('OAUTH_SCOPES')
                ]);

                try {
                    // Make the token request
                    $accessToken = $oauthClient->getAccessToken('authorization_code', [
                        'code' => $_GET['code']
                    ]);

                    // Save the access token and refresh tokens in session
                    // This is for demo purposes only. A better method would
                    // be to store the refresh token in a secured database
                    $tokenCache = new TokenCache();
                    $tokenCache->storeTokens(
                        $accessToken->getToken(),
                        $accessToken->getRefreshToken(),
                        $accessToken->getExpires()
                    );

                    // Redirect back to rooms page
                    return redirect()->route('selectLocation');
                } catch (IdentityProviderException $e) {
                    exit('ERROR getting tokens: ' . $e->getMessage());
                }
                exit();
            } elseif (isset($_GET['error'])) {
                exit('ERROR: ' . $_GET['error'] . ' - ' . $_GET['error_description']);
            }
        } catch(Exception $exception) {
            echo 'Error: ' . $exception;
        }
    }
}
