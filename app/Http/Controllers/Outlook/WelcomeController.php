<?php

declare(strict_types=1);

namespace App\Http\Controllers\Outlook;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class WelcomeController extends Controller
{
    /**
     * Welcoming site
     *
     * @return View
     */
    protected function welcome(): View
    {
        date_default_timezone_set('Europe/Berlin');

        $timeNow = strtotime(date(DATE_ISO8601));

        return view('welcome', [
            'timeNow' => $timeNow
        ]);
    }
}
