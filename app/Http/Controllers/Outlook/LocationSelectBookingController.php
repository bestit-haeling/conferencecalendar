<?php

declare(strict_types=1);

namespace App\Http\Controllers\Outlook;

use App\Http\Controllers\Controller;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Graph;

/**
 * Class LocationSelectBookingController
 *
 * @author Stefan Häling <stefan.haeling@bestit-online.de>
 *
 * @package App\Http\Controllers\Outlook
 */
class LocationSelectBookingController extends Controller
{
    /**
     * Location selection for booking
     *
     * @return mixed
     */
    protected function selectLocation()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache();

        $graph = new Graph();

        $timeNow = strtotime(date(DATE_ISO8601)) + 3600;

        try {
            $graph->setAccessToken($tokenCache->getAccessToken());
        } catch (Exception $e) {
            return redirect('/', '301', [
                'timeNow' => $timeNow,
            ]);
        }

        return view('selectLocation', [
            'timeNow' => $timeNow
        ]);
    }
}
