<?php

declare(strict_types=1);

namespace App\Http\Controllers\Outlook;

use App\Http\Controllers\Controller;
use Microsoft\Graph\Exception\GraphException;
use App\TokenStore\TokenCache;
use Illuminate\Support\Facades\Input;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

/**
 * Class BookingController
 *
 * @author Stefan Häling <stefan.haeling@bestit-online.de>
 *
 * @package App\Http\Controllers\Outlook
 */
class BookingController extends Controller
{
    /**
     * Quick booking to selected room
     *
     * @throws GraphException
     *
     * @param string $location
     * @param string $room
     *
     * @return mixed
     */
    protected function booking(string $location, string $room)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache();

        $graph = new Graph();

        $timeNow = strtotime(date(DATE_ISO8601));

        try {
            $graph->setAccessToken($tokenCache->getAccessToken());
        } catch (Exception $e) {
            return redirect('/', '301', [
                'timeNow' => $timeNow,
            ]);
        }

        $rooms = json_decode(file_get_contents('../rooms.json'), true);

        $roomId = $rooms[$location][$room];

        date_default_timezone_set('Europe/Berlin');

        $timeNowMinutes = date('i', $timeNow) / 60;

        $roomGetCalendarUrl = '/users/' . $roomId . '/events/';

        $date = date('Y-m-d');
        $startDateTime = $date . 'T06:00:00';
        $endDateTime = $date . 'T20:00:00';

        $roomGetCalendarUrlToday = '/users/' . $roomId . '/calendarView?startDateTime=' . $startDateTime
            . '&endDateTime=' . $endDateTime;

        try {
            $appointmentCalendar = $graph->createRequest('GET', $roomGetCalendarUrlToday)
                ->setReturnType(Model\Event::class)
                ->execute();
        } catch (GraphException $e) {
            return redirect('/', '301', [
                'timeNow' => $timeNow,
            ]);
        }

        if (is_array($appointmentCalendar)) {
            for ($i = 0; $i < sizeof($appointmentCalendar); $i++) {
                $appointmentArray[$i]['start'] = strtotime(
                    json_decode(json_encode($appointmentCalendar[$i]->getStart()), true)['dateTime']
                ) + 3600;
                $appointmentArray[$i]['end'] = strtotime(
                    json_decode(json_encode($appointmentCalendar[$i]->getEnd()), true)['dateTime']
                ) + 3600;
            }
        } else {
            $appointmentArray = [];
        }

        $nextAppointmentStart = null;
        $currentAppointmentEnd = null;

        sort($appointmentArray);

        $available = true;

        //check for current/ongoing event
        foreach ($appointmentArray as $appointment) {
            if ($appointment['start'] < $timeNow && $timeNow < $appointment['end']) {
                //event currently taking place
                $available = false;
                $nextAppointmentStart = $appointment['start'];
                $currentAppointmentEnd = $appointment['end'];
                break;
            } elseif (($appointment['start'] - $timeNow) <= 900 && ($appointment['start'] - $timeNow) >= 0) {
                //event in less than 15 minutes
                $nextAppointmentStart = $appointment['start'];
                $currentAppointmentEnd = $appointment['end'];
                $available = false;
                break;
            } elseif (($appointment['start'] - $timeNow) > 900) {
                //event in more than 15 minutes
                $nextAppointmentStart = $appointment['start'];
                $currentAppointmentEnd = $appointment['end'];
                $available = true;
                break;
            }
        }

        $appointmentStartTime = null;
        $appointmentEndTime = null;
        $calendar = null;

        $quick15 = Input::get('quick15');
        $quick30 = Input::get('quick30');
        $quick60 = Input::get('quick60');

        if ($quick15) {
            $appointmentStartTime = date(DATE_ISO8601);
            $appointmentEndTime = date(DATE_ISO8601, strtotime($appointmentStartTime) + 900);
        } elseif ($quick30) {
            $appointmentStartTime = date(DATE_ISO8601);
            $appointmentEndTime = date(DATE_ISO8601, strtotime($appointmentStartTime) + 1800);
        } elseif ($quick60) {
            $appointmentStartTime = date(DATE_ISO8601);
            $appointmentEndTime = date(DATE_ISO8601, strtotime($appointmentStartTime) + 3600);
        }

        $user = $graph->createRequest('GET', '/me')
            ->setReturnType(Model\User::class)
            ->execute();

        $userdisplayName = $user->getDisplayName();

        if ($appointmentStartTime && $appointmentEndTime) {
            $jsonData = [
                'subject' => $userdisplayName,
                'start' => [
                    'dateTime' => $appointmentStartTime,
                    'timeZone' => 'Europe/Berlin'
                ],
                'end' => [
                    'dateTime' => $appointmentEndTime,
                    'timeZone' => 'Europe/Berlin'
                ]
            ];

            try {
                $graph->createRequest('POST', $roomGetCalendarUrl)
                    ->setReturnType(Model\Event::class)
                    ->attachBody(json_encode($jsonData))
                    ->execute()
                ;
            } catch (GraphException $e) {
                return redirect('/', '301', [
                    'timeNow' => $timeNow,
                ]);
            }
            return redirect('booking/' . $location . '/' . $room);
        }

        return view('booking', [
            'appointmentStartTime' => $appointmentStartTime,
            'appointmentEndTime' => $appointmentEndTime,
            'nextAppointmentStart' => $nextAppointmentStart,
            'currentAppointmentEnd' => $currentAppointmentEnd,
            'available' => $available,
            'location' => $location,
            'room' => $room,
            'date' => $date,
            'timeNow' => $timeNow,
            'timeNowMinutes' => $timeNowMinutes
        ]);
    }
}
