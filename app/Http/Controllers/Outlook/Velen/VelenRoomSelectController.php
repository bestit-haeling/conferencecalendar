<?php

declare(strict_types=1);

namespace App\Http\Controllers\Outlook\Velen;

use App\Http\Controllers\Controller;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Graph;

class VelenRoomSelectController extends Controller
{
    /**
     * Room selection for each room of location Velen
     *
     * @return mixed
     */
    protected function selectVelen()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache();

        $graph = new Graph();

        $timeNow = strtotime(date(DATE_ISO8601)) + 3600;

        try {
            $graph->setAccessToken($tokenCache->getAccessToken());
        } catch (Exception $e) {
            return redirect('/', '301', [
                'timeNow' => $timeNow,
            ]);
        }

        return view('selectVelen', [
            'timeNow' => $timeNow
        ]);
    }
}
