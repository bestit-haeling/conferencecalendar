<?php

declare(strict_types=1);

namespace App\Http\Controllers\Outlook\Velen;

use App\Http\Controllers\Controller;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Exception\GraphException;

class VelenCalendarController extends Controller
{
    /**
     * Get room occupation for current day
     *
     * @throws GraphException
     *
     * @return mixed
     */
    protected function roomsVelen()
    {
        date_default_timezone_set('Europe/Berlin');

        $timeNow = strtotime(date(DATE_ISO8601));

        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache();

        $graph = new Graph();

        try {
            $graph->setAccessToken($tokenCache->getAccessToken());
        } catch (Exception $e) {
            return redirect('/', '301', [
                'timeNow' => $timeNow,
            ]);
        }

        $rooms = json_decode(file_get_contents('../rooms.json'), true);

        $roomIdAquarium = $rooms['Velen']['Aquarium'];
        $roomIdBistro = $rooms['Velen']['Bistro'];
        $roomIdConferenceVelen = $rooms['Velen']['Conference-Room'];
        $roomIdOrange = $rooms['Velen']['Orange-Room'];

        $date = date('Y-m-d');
        $startDateTime = $date . 'T06:00:00';
        $endDateTime = $date . 'T20:00:00';

        $query = [
            'startDateTime' => $startDateTime,
            'endDateTime' => $endDateTime
        ];

        $roomGetCalendarUrlAquarium = '/users/' . $roomIdAquarium . '/calendarView?' . http_build_query($query);
        $roomGetCalendarUrlBistro = '/users/' . $roomIdBistro . '/calendarView?' . http_build_query($query) ;
        $roomGetCalendarUrlConferenceVelen = '/users/' . $roomIdConferenceVelen . '/calendarView?'
            . http_build_query($query);
        $roomGetCalendarUrlOrange = '/users/' . $roomIdOrange . '/calendarView?' . http_build_query($query);

        $jsonArray = [
            "requests" => [
                [
                    "id" => "1",
                    "method" => 'GET',
                    "url" => $roomGetCalendarUrlAquarium
                ],
                [
                    "id" => "2",
                    "method" => "GET",
                    "url" => $roomGetCalendarUrlBistro,
                    "dependsOn" => ["1"],
                ],
                [
                    "id" => "3",
                    "method" => "GET",
                    "url" => $roomGetCalendarUrlConferenceVelen,
                    "dependsOn" => ["2"],
                ],
                [
                    "id" => "4",
                    "method" => "GET",
                    "url" => $roomGetCalendarUrlOrange,
                    "dependsOn" => ["3"],
                ]
            ]
        ];

        try {
            $batch = $graph->createRequest("POST", "/\$batch")
                ->setReturnType(Model\Event::class)
                ->attachBody(json_encode($jsonArray))
                ->execute();
        } catch (GraphException $e) {
            return redirect('/', '301', [
                'timeNow' => $timeNow,
            ]);
        }

        $batch = json_decode(json_encode($batch), true);

        $batch = $batch['responses'];

        if (isset($batch[0]['body']['value'])) {
            $calendarAquariumArray = $batch[0]['body']['value'];
        }

        if (isset($batch[1]['body']['value'])) {
            $calendarBistroArray = $batch[1]['body']['value'];
        }

        if (isset($batch[2]['body']['value'])) {
            $calendarConferenceVelenArray = $batch[2]['body']['value'];
        }

        if (isset($batch[3]['body']['value'])) {
            $calendarOrangeArray = $batch[3]['body']['value'];
        }

        $appointmentsAquarium = [];
        $appointmentsBistro = [];
        $appointmentsConferenceVelen = [];
        $appointmentsOrange = [];

        if (isset($calendarAquariumArray[0])) {
            for ($i = 0; $i < sizeof($calendarAquariumArray); $i++) {
                $appointmentStartTime = date('H:i', strtotime(
                    $calendarAquariumArray[$i]['start']['dateTime']
                ) + 3600);
                $appointmentEndTime = date('H:i', strtotime(
                    $calendarAquariumArray[$i]['end']['dateTime']
                ) + 3600);
                $appointmentsAquarium[] = [
                    'start' => $appointmentStartTime,
                    'end' => $appointmentEndTime
                ];
            }
        }
        if (isset($calendarBistroArray[0])) {
            for ($i = 0; $i < sizeof($calendarBistroArray); $i++) {
                $appointmentStartTime = date('H:i', strtotime(
                    $calendarBistroArray[$i]['start']['dateTime']
                ) + 3600);
                $appointmentEndTime = date('H:i', strtotime(
                    $calendarBistroArray[$i]['end']['dateTime']
                ) + 3600);
                $appointmentsBistro[] = [
                    'start' => $appointmentStartTime,
                    'end' => $appointmentEndTime
                ];
            }
        }
        if (isset($calendarConferenceVelenArray[0])) {
            for ($i = 0; $i < sizeof($calendarConferenceVelenArray); $i++) {
                $appointmentStartTime = date('H:i', strtotime(
                    $calendarConferenceVelenArray[$i]['start']['dateTime']
                ) + 3600);
                $appointmentEndTime = date('H:i', strtotime(
                    $calendarConferenceVelenArray[$i]['end']['dateTime']
                ) + 3600);
                $appointmentsConferenceVelen[] = [
                    'start' => $appointmentStartTime,
                    'end' => $appointmentEndTime
                ];
            }
        }
        if (isset($calendarOrangeArray[0])) {
            for ($i = 0; $i < sizeof($calendarOrangeArray); $i++) {
                $appointmentStartTime = date('H:i', strtotime(
                    $calendarOrangeArray[$i]['start']['dateTime']
                ) + 3600);
                $appointmentEndTime = date('H:i', strtotime(
                    $calendarOrangeArray[$i]['end']['dateTime']
                ) + 3600);
                $appointmentsOrange[] = [
                    'start' => $appointmentStartTime,
                    'end' => $appointmentEndTime
                ];
            }
        }

        sort($appointmentsAquarium);
        sort($appointmentsBistro);
        sort($appointmentsConferenceVelen);
        sort($appointmentsOrange);

        return view('roomsVelen', [
            'appointmentsAquarium' => $appointmentsAquarium,
            'appointmentsBistro' => $appointmentsBistro,
            'appointmentsConferenceVelen' => $appointmentsConferenceVelen,
            'appointmentsOrange' => $appointmentsOrange,
            'timeNow' => $timeNow
        ]);
    }
}
