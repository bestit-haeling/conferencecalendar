<?php

declare(strict_types=1);

namespace App\Http\Controllers\Outlook;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class RoomSelectController extends Controller
{
    /**
     * Select Locations for room Overview
     *
     * @return View
     */
    protected function roomSelect(): View
    {
        date_default_timezone_set('Europe/Berlin');

        $timeNow = strtotime(date(DATE_ISO8601));

        return view('roomLocationSelect', [
            'timeNow' => $timeNow
        ]);
    }
}
