<?php

declare(strict_types=1);

namespace App\Http\Controllers\Outlook;

use App\Http\Controllers\Controller;
use Microsoft\Graph\Exception\GraphException;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class UsersController extends Controller
{
    /**
     * Shows user and room names and IDs
     *
     * @throws GraphException
     *
     * @return mixed
     */
    protected function users()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }

        date_default_timezone_set('Europe/Berlin');

        $timeNow = strtotime(date(DATE_ISO8601));

        $tokenCache = new TokenCache();

        $graph = new Graph();

        try {
            $graph->setAccessToken($tokenCache->getAccessToken());
        } catch (Exception $e) {
            return redirect('/', '301', [
                'timeNow' => $timeNow,
            ]);
        }

        //Disable double quote sniff
        //phpcs:disable Squiz.Strings.DoubleQuoteUsage.NotRequired
        $userQueryParameters = [
            "\$select" => 'displayName, ID'
//            "\$filter" => "startswith(displayName , 'Orange')"
        ];
        //phpcs:enable
        $getUsersUrl = '/users?' . http_build_query($userQueryParameters);
        try {
            $users = $graph->createRequest('GET', $getUsersUrl)
                ->setReturnType(Model\User::class)
                ->execute();
        } catch (Exception $e) {
            return redirect('/', '301', [
                'timeNow' => $timeNow,
            ]);
        }

        return view('users', [
            'users' => $users,
            'timeNow' => $timeNow
        ]);
    }
}
