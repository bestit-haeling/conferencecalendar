<?php

declare(strict_types=1);

namespace app\Http\Controllers;

use Illuminate\Mail\Mailable;
use Microsoft\Graph\Graph;

class MailController extends Mailable
{
    public function sendMail()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache();

        $graph = new Graph();

        $graph->setAccessToken($tokenCache->getAccessToken());

        $mailingURL = 'me/mailFolders("sentItems")/messages?$select=sender,subject';

//        $graph->createRequest('GET', $mailingURL)
//            ->setReturnType(Mail)
//        ;

        return view('sendMail', []);
    }
}
