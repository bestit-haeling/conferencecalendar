@extends('layout')

@section('content')
    <div id="rooms" class="panel panel-default">
        <div class="container">
            <div class="panel-heading">
                <h1 align="center" class="panel-title m-2">Rooms Calendar</h1>
            </div>
        </div>
        <div class="container">
            <div class="panel-body">
                <div align="center" class="container p-2">
                    <label>Here are the events for today.</label>
                </div>
                <div class="container-fluid">
                    <div class="row text-center">
                        <div class="col-md-3">
                            <a role="button" class="text-dark" href="/booking/Velen/Aquarium">
                                <h5 class="border rounded">Aquarium:</h5>
                            </a>
                            <div class="mt-4">
                                @if (isset($appointmentsAquarium[0]))
                                    @foreach ($appointmentsAquarium as $appointment)
                                        @if (strtotime($appointment['start']) <= $timeNow && $timeNow <= strtotime($appointment['end']))
                                            <div class="text-danger">
                                                <label>{{ $appointment['start'] }} to {{ $appointment['end'] }}</label><br>
                                            </div>
                                        @else
                                            <label>{{ $appointment['start'] }} to {{ $appointment['end'] }}</label><br>
                                        @endif
                                    @endforeach
                                @else
                                    <p>No appointments today</p><br>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a role="button" class="text-dark" href="/booking/Velen/Bistro">
                                <h5 class="border rounded">Bistro:</h5>
                            </a>
                            <div class="mt-4">
                                @if (isset($appointmentsBistro[0]))
                                    @foreach($appointmentsBistro as $appointment)
                                        @if (strtotime($appointment['start']) <= $timeNow && $timeNow <= strtotime($appointment['end']))
                                            <div class="text-danger">
                                                <label>{{ $appointment['start'] }} to {{ $appointment['end'] }}</label><br>
                                            </div>
                                        @else
                                            <label>{{ $appointment['start'] }} to {{ $appointment['end'] }}</label><br>
                                        @endif
                                    @endforeach
                                @else
                                    <p>No appointments today</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a role="button" class="text-dark" href="/booking/Velen/Conference-Room">
                                <h5 class="border rounded">Conference-Room:</h5>
                            </a>
                            <div class="mt-4">
                                @if (isset($appointmentsConferenceVelen[0]))
                                    @foreach($appointmentsConferenceVelen as $appointment)
                                        @if (strtotime($appointment['start']) <= $timeNow && $timeNow <= strtotime($appointment['end']))
                                            <div class="text-danger">
                                                <label>{{ $appointment['start'] }} to {{ $appointment['end'] }}</label><br>
                                            </div>
                                        @else
                                            <label>{{ $appointment['start'] }} to {{ $appointment['end'] }}</label><br>
                                        @endif
                                    @endforeach
                                @else
                                    <p>No appointments today</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a role="button" class="text-dark" href="/booking/Velen/Orange-Room">
                                <h5 class="border rounded">Orange-Room:</h5>
                            </a>
                            <div class="mt-4">
                                @if (isset($appointmentsOrange[0]))
                                    @foreach($appointmentsOrange as $appointment)
                                        @if (strtotime($appointment['start']) <= $timeNow && $timeNow <= strtotime($appointment['end']))
                                            <div class="text-danger">
                                                <label>{{ $appointment['start'] }} to {{ $appointment['end'] }}</label><br>
                                            </div>
                                        @else
                                            <label>{{ $appointment['start'] }} to {{ $appointment['end'] }}</label><br>
                                        @endif
                                    @endforeach
                                @else
                                    <p>No appointments today</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
