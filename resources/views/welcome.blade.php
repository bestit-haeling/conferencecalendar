@extends('layout')

@section('header')
    <meta http-equiv="refresh" content="1; URL=/signin">
@endsection

@section('content')
    <div class="jumbotron">
        <h1 align="center">Welcome</h1>
        <h2 align="center">to the ConferenceCalendar</h2>
        <p align="center">
            First of all, please <a class="btn btn-lg btn-primary" href="/signin" role="button" id="connect-button">Connect to Outlook</a>.
        </p>
    </div>
@endsection
