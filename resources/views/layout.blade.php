<!DOCTYPE html>
<html>
<head>
    <title>ConferenceCalendar</title>
    <meta http-equiv="refresh" content="60"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    @yield('header')
</head>
<body>
<nav class="navbar navbar-fixed-top">
    <div class="container p-4">
        <button type="button" class="navbar-toggle btn btn-sm border-success" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            Menu
        </button>
        <h2 align="center" class="panel-title border accordion m-1 rounded">{{ date('H:i', $timeNow) }}</h2>
        <div id="navbar" class="container navbar-collapse collapse" data-toggle="false">
            <div class="navbar-header p-4">
                <ul style="list-style-type: none" class="navbar">
                    <li class="<?php echo (Request::url() == '/' ? 'active' : '');?>"><a href="/"><button class="btn btn-secondary"> Home</button></a></li>
                    <li class="<?php echo (Request::url() == '/rooms' ? 'active' : '');?>"><a href="/roomSelect"><button class="btn btn-secondary">Rooms</button></a></li>
                    <li class="<?php echo (Request::url() == '/booking' ? 'active' : '');?>"><a href="/selectLocation"><button class="btn btn-secondary">Room Booking</button></a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div class="container-fluid" role="main">
    @yield('content')
    {{-- bottom line --}}
    <div class="container-fluid border">
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
