@extends('layout')

@section('content')
    <div id="selectVelen" class="panel panel-default">
        <div class="panel-heading">
            <h1 align="center" class="panel-title">Velen</h1>
        </div>
        <div align="center" class="panel-body p-4">
            <h2 align="center">Please select the room you want to book.</h2>
            <p align="center">As of now, only Velen is supported.</p>
        </div>
        <div align="center" class="mb-4">
            <form align="center">
                <button class="btn btn-lg btn-primary" formaction="/booking/Velen/Aquarium" type="submit">Aquarium</button>
                <button class="btn btn-lg btn-primary" formaction="/booking/Velen/Bistro" type="submit">Bistro</button>
                <button class="btn btn-lg btn-primary" formaction="/booking/Velen/Conference-Room" type="submit">Conference-Room</button>
                <button class="btn btn-lg btn-primary" formaction="/booking/Velen/Orange-Room" type="submit">Orange-Room</button>
            </form>
        </div>
    </div>
@endsection