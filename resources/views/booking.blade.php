@extends('layout')

@section('content')
    <div id="booking" class="panel panel-default container">
        <div class="panel-heading m-4">
            <h2 align="center" class="panel-title">{{ $room }}</h2>
        </div>
        <div align="center" class="m-4">
            @if ($available === true)
                <div class="border border-success w-25 bg-success text-white p-2 pt-2">
                    <h2>Available</h2>
                </div>
                @if (($nextAppointmentStart - $timeNow) < 1800 && ($nextAppointmentStart - $timeNow) > 0) {{-- weniger als 30 Minuten bis nachster Termin --}}
                    <div class="m-4">
                        <h3 align="center">book now for:</h3>
                    </div>
                    <form align="center" class="m-4">
                        <input align="center" class="btn btn-lg btn-primary" type="submit" name="quick15" value="15 minutes">
                    </form>
                    @if ($nextAppointmentStart)
                        <div>
                            <p>The next appointment will start at {{ date('H:i', $nextAppointmentStart) }}.</p>
                        </div>
                    @else
                        <div>
                            <p>There are no appointments in this room today.</p>
                        </div>
                    @endif
                @elseif (($nextAppointmentStart - $timeNow) < 3600 && ($nextAppointmentStart - $timeNow) > 0) {{-- weniger als 60 Minuten bis nachster Termin --}}
                    <div class="m-4">
                        <h3 align="center">book now for:</h3>
                    </div>
                    <form align="center" class="m-4">
                        <input align="center" class="btn btn-lg btn-primary" type="submit" name="quick15" value="15 minutes">
                        <input align="center" class="btn btn-lg btn-primary" type="submit" name="quick30" value="30 minutes">
                    </form>
                    @if ($nextAppointmentStart)
                        <div>
                            <p>The next appointment will start at {{ date('H:i', $nextAppointmentStart) }}.</p>
                        </div>
                    @else
                        <div>
                            <p>There are no appointments in this room today.</p>
                        </div>
                    @endif
                @elseif (($nextAppointmentStart - $timeNow) >= 3600 || ($nextAppointmentStart - $timeNow) < 0) {{-- (mindestens 60 Minuten bis oder keine Termine mehr --}}
                    <div class="m-4">
                        <h3 align="center">book now for:</h3>
                    </div>
                    <form align="center" class="m-4">
                        <input align="center" class="btn btn-lg btn-primary" type="submit" name="quick15" value="15 minutes">
                        <input align="center" class="btn btn-lg btn-primary" type="submit" name="quick30" value="30 minutes">
                        <input align="center" class="btn btn-lg btn-primary" type="submit" name="quick60" value="60 minutes">
                    </form>
                    @if ($nextAppointmentStart)
                        <div>
                            <p>The next appointment will start at {{ date('H:i', $nextAppointmentStart) }}.</p>
                        </div>
                    @else
                        <div>
                            <p>There are no appointments in this room today.</p>
                        </div>
                    @endif
                @endif
            @elseif ($available === false)
                <div class="border border-danger w-25 bg-danger text-dark m-2 pt-2">
                    <h2>Unavailable</h2>
                </div>
                @if ($nextAppointmentStart)
                    <div>
                        <p class="p-4">The current appointment will end at {{ date('H:i', $currentAppointmentEnd) }}.</p>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection
