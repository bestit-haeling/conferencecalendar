@extends('layout')

@section('content')
    <div id="inbox" class="panel panel-default">
        <h1 align="center">You don't have the authority to carry out this operation.</h1>
    </div>
@endsection
