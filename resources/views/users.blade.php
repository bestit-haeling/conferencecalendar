@extends('layout')

@section('content')
    <div id="inbox" class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">RoomsCalendar</h1>
        </div>
        <div class="panel-body">
            Here are the people.
        </div>
        <div class="main-body">
            <?php for ($i = 0; $i < sizeof($users); $i++) {
                echo 'Name: ' . $users[$i]->getDisplayName() . ', ID: ' . $users[$i]->getID() . '<br>';
            } ?>
        </div>
    </div>
@endsection
