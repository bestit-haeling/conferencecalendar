@extends('layout')

@section('content')
    <div id="selectLocation" class="panel panel-default">
        <div class="panel-heading">
            <h1 align="center" class="panel-title">Location</h1>
        </div>
        <div align="center" class="panel-body p-4">
            <h2 align="center">Please select the location you are in.</h2>
            <p align="center">As of now, only Velen is supported.</p>
        </div>
        <div align="center" class="mb-4">
            <form align="center">
                <button class="btn btn-lg btn-primary" formaction="#" type="submit">Amstetten</button>
                <button class="btn btn-lg btn-primary" formaction="#" type="submit">Berlin</button>
                <button class="btn btn-lg btn-primary" formaction="#" type="submit">Siegburg</button>
                <button class="btn btn-lg btn-primary" formaction="/selectVelen" type="submit">Velen</button>
            </form>
        </div>
    </div>
@endsection